#!/bin/bash
LIST=`ls *.gnuplot`

for FILE in `echo $LIST`; do
gnuplot $FILE
EPS=`echo $FILE | awk -F"." '{print $1}'`
epstopdf ${EPS}.eps
done
