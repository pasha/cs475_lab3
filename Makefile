CC = gcc
OBJS = timer.o
FLAGS = -fopenmp -O3
DEBUGFLAG = -DDEBUG
SEQFLAGS = -O3 -lm

EXEC = merge_sort merge_sort_debug merge_sortNP merge_sortP merge_sortS

all: $(EXEC)

merge_sort: merge_sort.c $(OBJS)
	$(CC) $(SEQFLAGS)  -o $@ $< $(OBJS)

merge_sort_debug: merge_sort.c $(OBJS)
	$(CC) $(SEQFLAGS) $(DEBUGFLAG)  -o $@ $< $(OBJS)

merge_sortNP: merge_sortNP.c $(OBJS)
	$(CC) $(FLAGS)  -o $@ $< $(OBJS)

merge_sortP: merge_sortP.c $(OBJS)
	$(CC) $(FLAGS)  -o $@ $< $(OBJS)

merge_sortS: merge_sortS.c $(OBJS)
	$(CC) $(FLAGS)  -o $@ $< $(OBJS)

timer.o: timer.c
	$(CC) -O3 -o $@ -c $<

clean:
	rm -f $(OBJS) $(EXEC)
