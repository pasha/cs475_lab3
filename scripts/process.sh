#!/bin/bash

FLOAT=6
HOST=`hostname`
ITER=5
RANGE="1 2 3 4 5 6 7 8"
FILE=$1 #input
OUT=$2  #output
DATE=`date "+%Y-%m-%d-%H_%M_%S"`
MEDIAN=`echo \(5 + 1\) / 2 | bc`
PRETTY=$3

if [ "$PRETTY" == "yes" ] ; then 
printf "Threads | Time     | Speedup | Eff %%\n" >> $OUT
printf "===================================\n"   >> $OUT
fi

for NUM_THREADS in `echo $RANGE`; do
    M_TIME=`cat $FILE | grep "^$NUM_THREADS\ " | sort | head -${MEDIAN} | tail -1 | awk -F" = " '{print $2}'`
    MIN_TIME=`cat $FILE | grep "^$NUM_THREADS\ " | sort |  head -1 | awk -F" = " '{print $2}'`
    MAX_TIME=`cat $FILE | grep "^$NUM_THREADS\ " | sort |  tail -1 | awk -F" = " '{print $2}'`
    if [ "$NUM_THREADS" == "1" ] ; then
        SBASE=$M_TIME
    fi
    SCALE=`echo "scale=$FLOAT; $SBASE/$M_TIME" | bc`
    EFF=`echo "scale=$FLOAT; $SCALE * 100 / $NUM_THREADS" | bc`
    # echo "$NUM_THREADS $M_TIME"
    if [ "$PRETTY" == "yes" ] ; then 
    printf "%7d | %2.6f | %2.6f | %.1f \n" $NUM_THREADS $M_TIME $SCALE $EFF >> $OUT
    else
    echo "$NUM_THREADS $M_TIME $SCALE $EFF" >> $OUT
    # echo "$NUM_THREADS $M_TIME"
    # echo "$NUM_THREADS $SCALE"
    # echo "$NUM_THREADS $EFF"
    fi
done
