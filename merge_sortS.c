#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "timer.h"
#include "omp.h"
#include "common.h"

my_mergesort(int a[],int temp[], int low, int high) {
    int mid;
#if PRINTNUMTHREADS
    int tid = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    printf("Thread %d : Number of threads = %d Array section [%d:%d]\n", tid, nthreads, low, high);
#endif
    if(low<high) {
        mid=(low+high)/2;             //find the midpoint
        #pragma omp task firstprivate(a, temp, low, mid) if(mid - low > THRESHOLD)
        {
            my_mergesort(a,temp,low,mid);   //sort the first half
        }

        my_mergesort(a,temp,mid+1,high); //sort the second half

        #pragma omp taskwait
        merge(a,temp,low,high,mid);  //merge them togeather into one sorted list
    }
}

merge(int a[],int temp[], int low, int high, int mid){
 int i, j, k;
 i=low; j=mid+1; k=low;
 while((i<=mid)&&(j<=high)){
   if(a[i]<a[j]){
    temp[k]=a[i]; k++; i++;
   } else {
    temp[k]=a[j]; k++; j++;
   }
 }
 while(i<=mid){
   temp[k]=a[i]; k++; i++;
 }
 while(j<=high){
   temp[k]=a[j]; k++; j++;
 }
 for(i=low;i<k;i++) {
  a[i]=temp[i];
 }
}

int main(int argc, char **argv) {
 int LEN;
 double time;

 LEN=100000000;   
 // Command line argument: array length
 if ( argc > 1 ) LEN  = atoi(argv[1]);  
  int i, *x,*temp;
  x=(int *)malloc(sizeof(int)*LEN);
  temp=(int *)malloc(sizeof(int)*LEN);
  if(x==NULL || temp == NULL){
  printf("Out of memory"); exit(0);
  }

  //Fill the array to be sorted with random numbers
  for (i = 0; i < LEN; i++) 
    x[i] = rand() % LEN;

#if DEBUG
  printf("before sort:\n");
  for (i = 0; i < LEN; i++) printf("%d ", x[i]);
  printf("\n");
#endif

  initialize_timer ();  //We are just timing the sort
  start_timer();
  #pragma omp parallel
  {
      #pragma omp single
      my_mergesort(x,temp,0, (LEN-1));
  }
  stop_timer();
  time=elapsed_time();

#if DEBUG
  printf("after sort:\n");
  for (i = 0; i < LEN; i++) printf("%d ", x[i]);
  printf("\n");
#endif

  printf("elapsed time = %lf (sec)\n", time);
  return 0;
}
