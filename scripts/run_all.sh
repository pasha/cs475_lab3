#!/bin/bash

DATE=`date "+%Y-%m-%d-%H_%M_%S"`
DATE=../log/$DATE

mkdir -p $DATE

# ./run_s.sh ../sieve $DATE

# ./run_s.sh ../sieve1 $DATE

./run.sh ../merge_sortNP 0 $DATE
./run.sh ../merge_sortP  0 $DATE
./run.sh ../merge_sortS  0 $DATE
 
ln -s ../log/$DATE ../log/latest
