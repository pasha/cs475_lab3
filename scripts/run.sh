#!/bin/bash

HOST=`hostname`
ITER=5
APP=$1
APP_ITER="$2"
# reset 
APP_ITER=""
DIR=$3
RANGE="1 2 3 4 5 6 7 8"
OUT=
DATE=`date "+%Y-%m-%d-%H_%M_%S"`
TMP_OUT=11_tmp
APP_NAME=`echo $APP | tr -d '/' | tr -d '.'`
FILE_NAME=${APP_NAME}_${2}_out
FILE=${FILE_NAME}.txt

#SEQ=`echo $APP | sed 's/OMP/SEQ/'`
#./$SEQ $APP_ITER | grep ^[0-9] > seqlog

for NUM_THREADS in `echo $RANGE`; do
    export OMP_NUM_THREADS=$NUM_THREADS
    echo "export OMP_NUM_THREADS=$NUM_THREADS"
    COUNT=0
    while [ $COUNT -lt $ITER ] ; do
        echo $HOST \> $APP $APP_ITER
        $APP $APP_ITER | tee $TMP_OUT
        #cat $TMP_OUT | awk '{print v1 " = " $3}' v1=$NUM_THREADS >> $FILE
        cat $TMP_OUT  | grep "=" | awk -F "=" '{print $2}' | awk '{print $1}' | tr -d " " | awk '{print v1 " = " $1}' v1=$NUM_THREADS >> $DIR/$FILE
        cat $TMP_OUT | grep ^[0-9] > omplog
        #ERROR=`diff seqlog omplog | wc -l`
        #if [ "$ERROR" -ne "0" ] ; then
        #		exit 1
        #	fi
        let COUNT=COUNT+1
   done
done
rm -f $TMP_OUT seqlog omplog &> /dev/null

./process.sh $DIR/$FILE_NAME.txt $DIR/$FILE_NAME.dat no
./process.sh $DIR/$FILE_NAME.txt $DIR/$FILE_NAME.table yes
